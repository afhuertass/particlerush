package com.ParticleRush;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Particle-Rush";
		cfg.useGL20 = false;
		cfg.width = 384;
		cfg.height =640;
		
		new LwjglApplication(new ParticleRushGame(), cfg);
	}
}
