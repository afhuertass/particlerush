package com.ParticleRush;


import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class AssetsLoader {
public AssetManager assetManager; // asset manager que cargara todos los assets
public Image loading;
public LabelStyle labelStyle;
public boolean isLoading;
	AssetsLoader(){

		assetManager = new AssetManager(); // creamos el manager
		
		loadAtlas();
	}
	public void loadAtlas(){
		loadFont();
		assetManager.load("data/atlases/mainAtlas.pack", TextureAtlas.class);
		
	}
	public void loadGameAtlas(){
		assetManager.load("data/atlases/gameAtlas.pack", TextureAtlas.class);
	}
	public void disposeGameAtlas(){
		assetManager.unload("data/atlases/gameAtlas.pack");
	}
	public void loadAtlasReactor(){
		assetManager.load("data/atlases/reactorAtlas.pack", TextureAtlas.class);
	}
	public void disposeReactorAtlas(){
		assetManager.unload("data/atlases/reactorAtlas.pack");
	}
	public void loadStatsAtlas(){
		assetManager.load("data/atlases/statsAtlas.pack", TextureAtlas.class);
	}
	public void disposeStatsAtlas(){
		assetManager.unload("data/atlases/statsAtlas.pack");
	}
	
	public void loadFont(){
		assetManager.load("data/fuentes/sunnyside.fnt", BitmapFont.class);
		
	}
	public void loadLoading(){
		loading = new Image(new TextureRegionDrawable(assetManager.get("data/atlases/mainAtlas.pack",TextureAtlas.class).findRegion("loadingScreen")));
	}
	public void loadStyle(){
		this.labelStyle = new LabelStyle();
		labelStyle.font = this.assetManager.get("data/fuentes/sunnyside.fnt");
	}
}
