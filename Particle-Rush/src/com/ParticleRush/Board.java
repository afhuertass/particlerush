package com.ParticleRush;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Board {
float energy = 0;
ArrayList<Image> objetivosIm = new ArrayList<Image>();
ArrayList<Integer> objetivos = new ArrayList<Integer>(); // guarda los objetivos que salen // las particulas estaran identificadas por un numero del 1 al 9
Group combos;
Group bonuses;
Stage stage;
ParticleRushGame juego;
Image fondoGris;
Image marcoAzul;
Image EnergyLabel;
ImageButton menu; //solo un un puntero 
Image gameOver;
Label finalEnergy;
Label timeToShow;
Label totalEnergy;
Label parcialEnergy;
Label parcialEnergy2;
float totaltime;
float probabilidad = 0;
float probObjetivos = 1;
float inBajo = 0; 
float inAlto = 5;
float inBajo2 = 0;
float inAlto2 = 1;
int lifes;
int multiplicador;
int comboMarker = 10;
int comboCounter; // 10 significa sin combo 
boolean comboActive = false;
boolean isPlaying;
Group Lifes = new Group();
float x_canon = 58;
Random rnd = new Random();
float energyPerSec;
float energyIn;

	Board(Stage stg,ParticleRushGame juego){
		// esta clase maneja las bolitas activas, crea las nuevas, lleva el conteo del score
		// lleva la cuenta de los combos, coloca las animaciones de combos y scores
		
		this.stage = stg;
		combos = new Group();
		bonuses = new Group();
		stage.addActor(combos);
		stage.addActor(bonuses);
		lifes = 3;
		this.juego = juego;
		this.multiplicador = 1;
		updateLifes();
		LabelStyle la = new LabelStyle();
		la.font = juego.assetLoader.assetManager.get("data/fuentes/sunnyside.fnt");
		this.totalEnergy = new Label("",la);
		this.parcialEnergy = new Label("",la);
		this.parcialEnergy2 = new Label("",la);
		this.finalEnergy = new Label("",la);
		this.timeToShow = new Label("",la);
		stage.addActor(totalEnergy);
		totalEnergy.setPosition(248, 200);
		stage.addActor(parcialEnergy);
		stage.addActor(parcialEnergy2);
		isPlaying = true;
		loadGameOver();
		loadExternalData();
	}
	public void loadExternalData(){
		float contri = juego.dataManager.levelScientificQ *0.8f;
		this.energyPerSec = (float) (0.3*juego.dataManager.scientificHired*contri );
		this.energyIn = 1 +0.3f* juego.dataManager.levelEnergyIn;
	}
	
	public void loadGameOver(){
		this.gameOver = new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("gameOver")));
		gameOver.setPosition(40	, 678);
		gameOver.addAction(Actions.fadeOut(0.00001f));
		finalEnergy.addAction(Actions.fadeOut(0.00001f));
		timeToShow.addAction(Actions.fadeOut(0.00001f));
		stage.addActor(finalEnergy);
		stage.addActor(gameOver);
		//stage.addActor(gameOver.getImage());
		// cargamos la imagen;cH
		
		finalEnergy.setPosition(gameOver.getX()+270, gameOver.getY()+182+40);
		timeToShow.setPosition(gameOver.getX()+184, gameOver.getY()+ 125+30);
		gameOver.setTouchable(Touchable.disabled);
		this.addListeners();
		
	}
	public void updateBoard(float delta){
		//el metodo update board se llama desde el render() de la pantalla 
		//updateBoard() hace los calculos necesarios para agregar o no nuevos objetivos, o nuevas particulas
		if(!isPlaying) return;
		this.totaltime += delta;
		if(totaltime > inBajo2 && totaltime< inAlto2 ){ 
			inBajo2 = inAlto2; 
			inAlto2++;
			sumarEnergia2(this.energyPerSec); // energia de acuerdo a nivel r
		}
		//Gdx.app.log("particle", Float.toString(probabilidad));
		if(rnd.nextFloat() < 0.95){
			return;
		}
		float rndProb = rnd.nextFloat();
	
		if(rndProb < probabilidad){
			// entonces cree una nueva particula
			nuevaParticle();
			
			probabilidad = 0;
		}else {
			
			probabilidad += 0.001;
			//Gdx.app.log("particle","no creada" + Float.toString(probabilidad));
		}
		if(totaltime < 30 ){
			probabilidad = 0.20f; // si el tiempo en menor que 30 segundo la probabilidad no puede ser mayor a 30
		}
		if(totaltime > 30 && totaltime < 50 ) {
			probabilidad = 0.30f;  // esto es perfectamente ajustable pero el sistema es mas o menos claro ahora
			//Gdx.app.log("particle", "30 segundos 30%");
			
		}
		if(totaltime > 50 ){
			probabilidad = 0.50f;  // la maxima prob sera del 0.5 // en el 50% de las ocasiones que se llame esta funcion se creara una 
			// nueva particula
		}
		
		// ahora el sistema de nuevos objetivos
		// debe funcionar de manera similar pero la probabilidad debe ser mucho menor;
		// y hasta un marximo de 4 objetivos
		
		if(totaltime > inBajo && totaltime< inAlto ){
			rndProb = rnd.nextFloat();
			//Gdx.app.log("particle", "next objetive" + Float.toString(probObjetivos));
			
			if(rndProb < probObjetivos){
				
				agregarObjetivo();
				
				inBajo = inAlto;
				inAlto += 5;
				
			}
		}
		
		
	}
	public void nuevaParticle(){
		EnergyLabel.remove();
		marcoAzul.remove();
		new Particle(this).setX(x_canon); // eso es todo
		x_canon += 100;
		stage.addActor(marcoAzul);
		stage.addActor(EnergyLabel);
		for(Image obj: objetivosIm){
			obj.remove();
			stage.addActor(obj);
		}
		for(Actor img: Lifes.getChildren()){
			//Gdx.app.log("particle", "hoy la vamos a tumbar");
			img.remove(); 
			Lifes.addActor(img);
		}
		stage.addActor(Lifes);
		parcialEnergy.remove();
		parcialEnergy2.remove();
		totalEnergy.remove();
		stage.addActor(parcialEnergy);
		stage.addActor(totalEnergy);
		stage.addActor(parcialEnergy2);
		menu.remove();
		stage.addActor(menu);
		combos.remove();
		stage.addActor(combos);
		bonuses.remove();
		stage.addActor(bonuses);
		// el creador de Particle() genera un nuevo actor que es agregado al escenario y ya 
		// no hace falta tener una colecci�n almacenando todas las particulas
		if(x_canon > (600)){
			x_canon =60;
		}
	}
	public void agregarObjetivo(){
		// bueno
		// agregar un objetivo signifca, nueva cosa para mostrar y a la lista de objetivos
		int nuevoObjetivo = rnd.nextInt(8); 
		// a�adir un objetivo es, mostrarlo en la pantalla de abajo y a�adirlo a la lista
		//primero lo facil a�adirlo a la lista 
		Gdx.app.log("particle objetivos size", Integer.toString(objetivos.size()));
		if(objetivos.size() >4) return;
		
		
		if(this.objetivos.contains(nuevoObjetivo)) return;
		objetivos.add(nuevoObjetivo); // agregado
		
		switch(nuevoObjetivo){
		case 0:
			
			objetivosIm.add(new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("electron")))); 
			objetivos.add(0);
			break; 
		case 1:
			
			objetivosIm.add( new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("proton")))); 
			objetivos.add(1);
			break;	
		case 2:
			
			objetivosIm.add( new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("down")))); 
			objetivos.add(2);
			break;
		case 3:
			objetivosIm.add( new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("top"))));
			objetivos.add(3);
			break;
		case 4:
			objetivosIm.add( new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("strange"))));
			objetivos.add(4);
			break;	
		case 5:
			objetivosIm.add( new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("charm"))));
			objetivos.add(5);
			break;
		case 6:
			objetivosIm.add( new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("neutron"))));
			objetivos.add(6);
			break;
		case 7:
			objetivosIm.add( new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("antineutrino"))));
			objetivos.add(7);
			break;	
		}
		switch(objetivosIm.size()-1){
		case 0:
			objetivosIm.get(objetivosIm.size()-1).setPosition(StaticInfo.Objetivo1.x, StaticInfo.Objetivo1.y);
			break;
		case 1:
			
			objetivosIm.get(objetivosIm.size()-1).setPosition(StaticInfo.Objetivo2.x, StaticInfo.Objetivo2.y);
			break;
		case 2:
			objetivosIm.get(objetivosIm.size()-1).setPosition(StaticInfo.Objetivo3.x, StaticInfo.Objetivo3.y);
			break;
		case 3:
			objetivosIm.get(objetivosIm.size()-1).setPosition(StaticInfo.Objetivo4.x, StaticInfo.Objetivo4.y);
			break;	
		}
		Gdx.app.log("particle", "OBJETIVO");
		objetivosIm.get(objetivosIm.size()-1).setScale(0.8f);
		
		objetivosIm.get(objetivosIm.size()-1).addAction(Actions.fadeOut(0.000001f)); // desaparecer a toda
		objetivosIm.get(objetivosIm.size()-1).addAction(Actions.fadeIn(0.5f)); // aparecer coolmente 
		
		stage.addActor(objetivosIm.get(objetivosIm.size()-1));
		
	}
	public void updateLifes(){
	
		for(int i = 0; i< lifes; i++){
		
			Lifes.addActor( new Image(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("lifes"))) );
			Lifes.getChildren().get(i).setPosition(656-(70*i), 230);;
		}
		stage.addActor(Lifes);
		
	}
	public void setGame(ParticleRushGame juego){
		this.juego = juego;
	}
	public void setBackground(Image back){
		fondoGris = back;
	}
	public void setFrame(Image frame){
		marcoAzul = frame;
	}
	public void setEnergy(Image osE){
		this.EnergyLabel = osE;
	}
	
	public void touched(int indicador,float x, float y){
		if(this.objetivos.contains(indicador)){
			// sumar puntos
			sumarEnergia(indicador);
		}else {
			// quitar vida
			// si vidas  = 0 game over
			restarVida();
		}
		if(indicador == 6) return;
		if(this.comboMarker == 10){
			comboMarker = indicador;
			comboCounter = 1;
			return;
		}
		if(comboMarker == indicador){
			comboCounter++;
			if(comboCounter == 3 || comboCounter ==5 ) {
				showCombos(x,y);
			}
		}else {
			comboMarker = indicador;
			comboCounter = 1;
		}
	}
	public void showCombos(float x, float y){
		Image comb;
		boolean is3 = false;
		if(comboCounter == 3) is3=true;
		if(is3) {
			comb = new Image(getComboDrawable("3"));
		} else {
			comb = new Image(getComboDrawable("5"));
			comboMarker = 10;
			comboCounter = 0;
		}
		comb.setScale(0.5f);
		if(comb.getWidth() < 100) comb.rotate(-90);
		if(x > 500) x-=90;
		comb.setPosition(x, y);
		comb.addAction(Actions.moveTo(x, y+100,1f));
		comb.addAction(Actions.fadeOut(1f) );
		combos.addActor(comb);
	}
	public TextureRegionDrawable getComboDrawable(String num){
		switch(comboMarker){
		case 0:
			return new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack" , TextureAtlas.class ).findRegion("cElectron"+num) );
			//break;
		case 1:
			return new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack" , TextureAtlas.class ).findRegion("cProton"+num) );
			
		case 2:
			return new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack" , TextureAtlas.class ).findRegion("cDown"+num) );
		case 3:
			return new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack" , TextureAtlas.class ).findRegion("cUp"+num) );
		case 4:
			return new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack" , TextureAtlas.class ).findRegion("cStrange"+num) );
		case 5:
			return new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack" , TextureAtlas.class ).findRegion("cCharm"+num) );
		case 6:
			return new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack" , TextureAtlas.class ).findRegion("cNeutron"+num) );
		case 7:
			return new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack" , TextureAtlas.class ).findRegion("cNeutrino"+num) );
		case 8:
			break;
		}
		return null;
	}
	public void reachEnd(int indicat){
		if(this.objetivos.contains(indicat)){
			restarVida();
		}
	}

	public void restarVida(){
		if(Lifes.getChildren().size == 0)return;
		if(Lifes.getChildren().get(Lifes.getChildren().size -1 ).getActions().size != 0 ){
			Gdx.app.log("particle", "NO MUERE");
			return; // si acciones corriendo en dicho objetivo no haga nada, es decir que no se pierden vidas
			// mientras se esta perdiendo una vida 
		}
		this.lifes -= 1;
		if(lifes < 0) return;
		if(lifes == 0) isPlaying = false;
		Lifes.getChildren().get(Lifes.getChildren().size -1).addAction(Actions.sequence(Actions.fadeOut(0.6f), Actions.run(new Runnable(){
			@Override
			public void run(){
				
				Lifes.getChildren().get(Lifes.getChildren().size -1).remove();
				//Lifes.removeActor(Lifes.getChildren().get(Lifes.getChildren().size-1));
				if(lifes == 0){
					// game over
					shotGameOver();
				}
			}
		}) ));
		
	}
	public void shotGameOver(){
		this.externActions();
		isPlaying = false;
		
		gameOver.addAction( Actions.sequence(Actions.fadeIn(0.55f),Actions.run(new Runnable(){
			@Override
			public void run(){
				gameOver.setTouchable(Touchable.enabled);
			}
		}) ) );
		finalEnergy.setText(Float.toString((float) (Math.rint(energy*100)/100)));
		finalEnergy.addAction( Actions.fadeIn(0.55f));
		float pa = Math.round(totaltime);
		timeToShow.setText(Float.toString(pa));
		timeToShow.addAction( Actions.fadeIn(0.55f));
		gameOver.remove();
		finalEnergy.remove();
		timeToShow.remove();
		stage.addActor(gameOver);
		stage.addActor(finalEnergy);
		stage.addActor(timeToShow);
	}
	public void setMenu(ImageButton me){
		this.menu = me;
	}
	public void addListeners(){
		gameOver.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
				// bueno entonces debe reniciar el juego
				gameOver.setTouchable(Touchable.disabled);
				hideGameover();
				return false;
			}
		});
	}
	public void hideGameover(){
		for(Actor a: this.objetivosIm){
			a.remove();
		}
		gameOver.addAction(Actions.sequence(Actions.fadeOut(0.5f) , Actions.run(new Runnable(){
			@Override
			public void run(){
				// reiniciar
				
				reset();
				
			}
		})));
		finalEnergy.addAction(Actions.fadeOut(0.5f));
		timeToShow.addAction(Actions.fadeOut(0.5f));
	}
	public void reset(){
		// restaurar vidas:
		lifes = 3;
		Lifes.clear();
		updateLifes();
		//limpiar objetivos
		this.objetivosIm.clear();
		this.objetivos.clear();
		// comenzar a jugar 
		this.isPlaying = true;
		this.energy = 0;
		this.inBajo = 0 ; this.inAlto = 10;
		this.inBajo2 = 0 ; this.inAlto2 =1;
		this.probabilidad = 0;
		this.totaltime = 0;
		this.totalEnergy.setText("");
	}
	public void sumarEnergia(int i){
		float sco = 20;
		switch(i){
		case 0:
			sco = 10*this.multiplicador*this.energyIn;
			break;
		case 1:
			sco = 20*this.multiplicador*this.energyIn;
			break;	
		case 2:
			sco = 25*this.multiplicador*this.energyIn;
			break;
		case 3:
			sco = 30 *this.multiplicador*this.energyIn;
			break;
		case 4:
			sco = 40*this.multiplicador*this.energyIn;
			break;
		case 5:
			sco = 40*this.multiplicador*this.energyIn;	
			break;
		case 6:
			sco = 60*this.multiplicador*this.energyIn;	
			break;
		case 7:	
			sco = 40*this.multiplicador*this.energyIn;	
			break;	
			
		}
		// ahora 
		energy += sco;
		this.parcialEnergy.setText("+"+Float.toString(sco));
		parcialEnergy.addAction(Actions.run(new Runnable(){
			@Override
			public void run() {
				parcialEnergy.setPosition(266, 180);
			}
		}));
		parcialEnergy.addAction(Actions.sequence(Actions.fadeIn(0.0001f), Actions.fadeOut(0.7f) ));
		
		parcialEnergy.addAction(Actions.moveTo(266, 130, 0.7f));
		totalEnergy.setText(Float.toString((float) (Math.rint(energy*100)/100)));
	}
	public void sumarEnergia2(float sum){
		energy += sum;
		sum =(float)  Math.rint(sum*100)/100;
		this.parcialEnergy2.setText("+"+Float.toString(sum));
		parcialEnergy2.addAction(Actions.run(new Runnable(){
			@Override
			public void run() {
				parcialEnergy2.setPosition(266, 230);
			}
		}));
		parcialEnergy2.addAction(Actions.sequence(Actions.fadeIn(0.0001f), Actions.fadeOut(0.9f) ));
		
		parcialEnergy2.addAction(Actions.moveTo(266, 280, 0.9f));
		totalEnergy.setText(Float.toString((float) (Math.rint(energy*100)/100)));
	}
	public void hideElements(){
		for(Actor a : stage.getActors()){
			a.addAction(Actions.fadeOut(0.6f));
		}
		
	}
	public void bonusEffect(int type, Image bonus){
		switch(type){
		case 0: 
			this.multiplicador =2;
			break;
			
		case 1:
			this.multiplicador = 3;
			break;
		case 2:
			// matar todas
			killAll();
			bonus.remove();
			break;
		}
		bonus.setPosition(520, 116);
		bonus.addAction(Actions.sequence(Actions.fadeOut(8f), Actions.run(new Runnable(){
			@Override
			public void run(){
				multiplicador = 1;
			}
		}) )); 
	}
	public void killAll(){
		float sco = 0;
		for(Actor a :stage.getActors()){
			if(a instanceof Particle){
				// si son particulas 
				sco += 60*this.multiplicador*this.energyIn;	
				((Particle) a).manualTouch();
			}
		}
		this.sumarEnergia2(sco);
		
	}
	public void externActions(){
		// externActions debe realizar las acciones exteriores, tales como aumentar los puntos totales 
		// si se desbloquean combos agregarlos 
		juego.dataManager.earnEnergy(energy);
		
	}
}
