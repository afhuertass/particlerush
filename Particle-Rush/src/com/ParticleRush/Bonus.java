package com.ParticleRush;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;


public class Bonus extends Actor{
	int type;
	float y_vel;
	public boolean active = false;
	Image bonus;
	Random rnd = new Random();
	Particle parent;
	Bonus(Particle p){
		parent = p;
	loadImageB();
	addListener();

	y_vel = rnd.nextFloat()*-600;
	bonus.setY(1250);
	bonus.setX(rnd.nextFloat()*600 -20);
	parent.boardPointer.stage.addActor(this);
	parent.boardPointer.bonuses.addActor(bonus);
	
	}
	@Override
	public void act(float delta){
		super.act(delta);
		
		bonus.setY(y_vel*delta + bonus.getY());
		if(bonus.getY() < 200 && !active ){
			bonus.remove();
		}
	
	}
	private void loadImageB(){
		switch(rnd.nextInt(3)){
		case 0:
			type = 0;
			bonus = new Image(new TextureRegionDrawable(parent.boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("x2") ));
			break;
		case 1:
			type =1;
			bonus = new Image(new TextureRegionDrawable(parent.boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("x3") ));
			break;
		case 2:
			type = 2;
			bonus = new Image(new TextureRegionDrawable(parent.boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("kill") ));
			break;
		}
		bonus.setScale(0.7f);
	}
	private void addListener(){
		
		bonus.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button){ 
				// lo que pase al tocarlas depende del tipo 
				y_vel = 0;
				parent.boardPointer.bonusEffect(type, bonus);
				active = true;
				return true;
			}
		});
			
		
	}
	
}
