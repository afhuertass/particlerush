package com.ParticleRush;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class GameScreen implements Screen{
ParticleRushGame juego;
Board board;
Stage stage;
Group gameScreenActors;
Image backGround; // la lista de actores que van en la pantalla
Image backGroundGray;  // seran cargados en el evento Show;
Image energyImage;
ImageButton menu;
boolean isLoaded= false;
	GameScreen(ParticleRushGame game){
	this.juego = game;
	juego.assetLoader.loadGameAtlas();
}
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
        juego.assetLoader.assetManager.update();
        if(juego.assetLoader.assetManager.isLoaded("data/atlases/gameAtlas.pack") && !isLoaded ){
        	// si ya cargo el atlas, 
        	
        	this.loadElements();
        	isLoaded = true; 
        }
        if(juego.assetLoader.assetManager.isLoaded("data/fuentes/sunnyside.fnt")){
        	juego.assetLoader.loadStyle();
        }
        if(isLoaded){
		board.updateBoard(delta); 
        }
        stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		stage.setViewport(720, 1280, true);
		stage.getCamera().position.set(stage.getCamera().position.x - stage.getGutterWidth(), stage.getCamera().position.y,0);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		stage.addActor(juego.assetLoader.loading);
		juego.assetLoader.loadGameAtlas();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	public void loadElements(){
		gameScreenActors = new Group();
		this.backGround = new Image( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("gameScreen")));
	this.backGroundGray  = new Image( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("backGroundGame")));
	backGroundGray.setPosition(10, 10); // fijas posicion.
	this.menu = new ImageButton(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack", TextureAtlas.class).findRegion("menu_reactor")), 
			new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack", TextureAtlas.class).findRegion("menu_reactorOn"))); 
	this.energyImage = new OscilatingText( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("energyGameScreen")));
	/// dar las posiciones
	energyImage.setPosition(24, 162);
	backGroundGray.setPosition(40+backGroundGray.getHeight(), 288);
	//backGroundGray.setOrigin(backGroundGray.getWidth()/2, backGroundGray.getHeight()/2);
	backGroundGray.rotate(90);
	menu.setPosition(525, 24);
	gameScreenActors.addActor(backGroundGray);
	this.gameScreenActors.addActor(backGround);  gameScreenActors.addActor(energyImage); gameScreenActors.addActor(menu);
	juego.assetLoader.loading.remove();
	gameScreenActors.addAction(Actions.fadeOut(0.00001f));gameScreenActors.addAction(Actions.fadeIn(0.6f));
	stage.addActor(gameScreenActors);
	// creamos el Board;
	board = new Board(stage,this.juego);
	board.setGame(juego);
	board.setBackground(backGroundGray); 
	board.setFrame(backGround);
	board.setEnergy(energyImage);
	board.setMenu(menu);
	addListeners();
	backGround.setTouchable(Touchable.disabled);
	}
	public void addListeners(){
		menu.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				juego.mainScreen.isLoaded = false;
				board.hideElements();
				stage.addAction(Actions.fadeOut(0.6f));
				gameScreenActors.addAction(Actions.sequence(Actions.fadeOut(0.6f) ,  Actions.run(new Runnable(){
					@Override
					public void run(){
						juego.assetLoader.disposeGameAtlas();
						juego.setScreen(juego.mainScreen);
					}
				})));
				
				
			}
		});
	}
}
