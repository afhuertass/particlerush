package com.ParticleRush;




import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MainScreen implements Screen {
Stage stage;
Group elements;

Image fondo;
ImageButton play;
ImageButton reactor;
ImageButton stats;
ParticleRushGame juego;
OscilatingText cabe;
boolean isLoaded = false;
float totalTime;
MainScreen(ParticleRushGame game){
	this.juego = game;
	Gdx.app.log("particle", "create");
}
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
        //Gdx.app.log("particle", "fuck dont load");
  
		if(juego.assetLoader.assetManager.update()){
			// si termino de cargar:
			
			juego.assetLoader.loadLoading();
			if(!isLoaded){
				Gdx.app.log("particle", "fuck dont load");
				isLoaded = true;
				LoadButtons();
				}
		
			stage.act(delta);
			stage.draw();
		
		}else {
			// aqui deberia poner la pantalla de cargando 
			//Gdx.app.log("particle", "fuck dont load");
		}
	}

	public void LoadButtons(){
		// una vez cargados los recursos creamos los botones
		Gdx.app.log("particle", "fuck dont load");
		play = new ImageButton( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/mainAtlas.pack", TextureAtlas.class).findRegion("playOff")), 
				new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/mainAtlas.pack", TextureAtlas.class).findRegion("playOn")));
		
		reactor = new ImageButton( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/mainAtlas.pack", TextureAtlas.class).findRegion("reactorOff")), 
				new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/mainAtlas.pack", TextureAtlas.class).findRegion("reactorOn")));
		
		stats = new ImageButton( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/mainAtlas.pack", TextureAtlas.class).findRegion("statsOff")), 
				new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/mainAtlas.pack", TextureAtlas.class).findRegion("statsOn")));
		
		
		fondo = new Image( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/mainAtlas.pack",TextureAtlas.class).findRegion("mainScreen")));
		cabe = new OscilatingText(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/mainAtlas.pack",TextureAtlas.class).findRegion("tituloMain")));
		cabe.setPosition(66, 918);
	
		play.setPosition(42, 632);
		reactor.setPosition(42, 396); stats.setPosition(42, 182);
		
		elements = new Group();
		elements.addActor(fondo);
		elements.addActor(play); elements.addActor(reactor); elements.addActor(stats); elements.addActor(cabe); 
		elements.addAction(Actions.fadeOut(0.00001f));elements.addAction(Actions.fadeIn(0.6f));
		addListeners();
		stage.addActor(elements);
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		stage.setViewport(720, 1280, true);
		stage.getCamera().position.set(stage.getCamera().position.x - stage.getGutterWidth(), stage.getCamera().position.y,0);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	public void addListeners(){
		play.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				if(juego.gameScreen == null){
					elements.addAction(Actions.sequence(Actions.fadeOut(0.6f) ,  Actions.run(new Runnable(){
						@Override
						public void run(){
							juego.gameScreen = new GameScreen(juego);
							juego.setScreen(juego.gameScreen);
						}
					})));
					
				}else {
					
					juego.gameScreen.isLoaded = false;
					
					elements.addAction(Actions.sequence(Actions.fadeOut(0.6f) ,  Actions.run(new Runnable(){
						@Override
						public void run(){
							
							juego.setScreen(juego.gameScreen);
							
						}
					})));
					
				}
			}
		});
		reactor.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				if(juego.reactorScreen == null){
					elements.addAction(Actions.sequence(Actions.fadeOut(0.6f) ,  Actions.run(new Runnable(){
						@Override
						public void run(){
							juego.reactorScreen = new ReactorScreen(juego);
							juego.setScreen(juego.reactorScreen);
						}
					})));
				
				}else {
					juego.reactorScreen.isLoaded = false;
					
					elements.addAction(Actions.sequence(Actions.fadeOut(0.6f) ,  Actions.run(new Runnable(){
						@Override
						public void run(){
							juego.setScreen(juego.reactorScreen);
							
						}
					})));
					
				}
			}
		});
		stats.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				if(juego.statsScreen == null){
					elements.addAction(Actions.sequence(Actions.fadeOut(0.6f) ,  Actions.run(new Runnable(){
						@Override
						public void run(){
							juego.statsScreen = new StatsScreen(juego);
							juego.setScreen(juego.statsScreen);
						}
					})));
				
				}else {
					juego.statsScreen.isLoaded = false;
					elements.addAction(Actions.sequence(Actions.fadeOut(0.6f) ,  Actions.run(new Runnable(){
						@Override
						public void run(){
							juego.setScreen(juego.statsScreen);
							
						}
					})));
					
				}
			}
		});
	}
}
