package com.ParticleRush;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

public class OscilatingText extends Image {
float timeAlive = 0;
float x_scale;
float y_scale;
float x;
float x0;
public float omega_x  = 3;
public float omega_y = 3;
boolean rotated = false;
Image text;
	OscilatingText(Drawable dra){
		// esta clase hereda de imagees
		text = new Image(dra);
		
	}
	@Override
	public void addAction(Action action){
		super.addAction(action);
		text.addAction(action);
	}
	@Override
	public void act(float delta){
		super.act(delta); // act
		timeAlive += delta;
		x_scale = (float) (0.95 + Math.sin(omega_x*timeAlive)*0.05 );
		y_scale = (float) (0.95 + -1*Math.sin(omega_y*timeAlive)*0.05 );
		if(!rotated){
			x = x0 + text.getImageWidth()*(1-x_scale);
			this.text.setScale(x_scale, y_scale);
		}else {
			x = x0 + text.getImageHeight()*(1-x_scale);
			this.text.setScale(y_scale, x_scale);
		}
		
		this.text.setX(x/2);
	}
	@Override
	public void rotate(float angle){
		text.rotate(angle);
		rotated = true;
	}
	@Override
	public void draw(SpriteBatch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		text.draw(batch, parentAlpha);
	}
	@Override
	public void setPosition(float x, float y){
		super.setPosition(x, y);
		text.setPosition(x, y);
		x0 = x;
	}
	public Image getImage(){
		return text;
	}
	public void changeOmegas(float wx,float wy) {
		omega_x = wx;
		omega_y = wy;
	}
}
