package com.ParticleRush;


import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Particle  extends Image{
// esta clase debe ser un actor, capaz de recibir los eventos, recibe como parametro de creacion
	// el board, para que una vez se destruya el board reciba la información y actue conforme a ello.
	// el tipo de bola creada es aleatoria, y manejada por el constructor de la clase particula
	// por su parte dentro del board, existe cierta probabilidad de crear una nueva particula 
	// cuando la probabilidad cae, entonces se creea una nueva particula que es anexada en el stage
	int type; 
	float omega; // velocidad angular
	float y_velocidad;
	boolean endReached = false;
	boolean touched = false;
	boolean activateParticle = false;
	Board boardPointer;
	Image particula;
	Random rnd = new Random();
	ParticleEffect atDeath;
	
	Particle(Board board){
		// creacion de la particula / que porcierto es un actor
	float x_inicial = rnd.nextFloat()*600 -20; 
		this.boardPointer = board;
	getImage(); /// cargamos alguna particula aleatoria 
	
	board.stage.addActor(this);
	board.stage.addActor(particula); 
	
	y_velocidad = -500*rnd.nextFloat();
	Gdx.app.log("particle", Float.toString(y_velocidad));
	if(y_velocidad > -300){
		y_velocidad = -400;
	}
	particula.setPosition(x_inicial, 1254);
	particula.setScale(0.7f);
	particula.setOrigin(particula.getWidth()/2, particula.getHeight()/2);
	omega = rnd.nextFloat()* 1000;
	if(rnd.nextInt(2) == 0){
		omega *=-1;
	}
	if(rnd.nextFloat() < 0.1){
		new Bonus(this);
	}
	adListener();
	
	}
	public void getImage(){
		int sele;
		TextureRegionDrawable region = null;
		
		sele = rnd.nextInt(8);
		switch(sele){
		case 0:
			
			region = new TextureRegionDrawable(boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("electron"));
			particula = new Image(region);type = 0;
			break;
		case 1:
			region = new TextureRegionDrawable(boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("proton"));
			particula = new Image(region);type = 1;
			break;
		case 2:
			region = new TextureRegionDrawable(boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("down"));
			particula = new Image(region);type = 2;
			break;
		case 3:
			region = new TextureRegionDrawable(boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("top"));
			particula = new Image(region);type = 3;
			break;
		case 4:
			region = new TextureRegionDrawable(boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("strange"));
			particula = new Image(region);type = 4;
			break;
		case 5:
			region = new TextureRegionDrawable(boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("charm"));
			particula = new Image(region);type = 5;
			break;
		case 6:
			region = new TextureRegionDrawable(boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("neutron"));
			particula = new Image(region);type = 6;
			break;
		case 7:
			region = new TextureRegionDrawable(boardPointer.juego.assetLoader.assetManager.get("data/atlases/gameAtlas.pack",TextureAtlas.class).findRegion("antineutrino"));
			particula = new Image(region);type = 7;
			break;
			
		}
		
	}
	@Override
	public void act(float delta){
		super.act(delta);
		
		particula.setY(particula.getY()+delta*this.y_velocidad);
		particula.rotate(omega*delta);
		if(particula.getY() < 200 && !endReached){
			endReached = true;
			particula.addAction(Actions.fadeOut(0.5f));
			//boardPointer.reachEnd(type);
			
		}
		if(particula.getY() < 200){
			particula.remove();
		}
	
	}
	@Override
	public void draw(SpriteBatch batch, float parentAlpha){
		super.draw(batch, parentAlpha);
		if(activateParticle){
			atDeath.draw(batch);
		}
	}
	public void adListener(){
		Gdx.app.log("particula", "me lo aaaaaaaaaa");
		
		particula.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button){
				//Gdx.app.log("particle","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
				if(touched) return true;
				
				touched = true;
				particula.setTouchable(Touchable.disabled);
				y_velocidad = 0;
				particula.addAction(Actions.fadeOut(0.4f));
				boardPointer.touched(type, particula.getX(), particula.getY());
				
				return true;
			}
		});
	}

	@Override
	public void setX(float x){
		particula.setX(x);
	}
	public void manualTouch(){
		touched = true;
		particula.setTouchable(Touchable.disabled);
		y_velocidad = 0;
		particula.addAction(Actions.sequence(Actions.fadeOut(0.4f), Actions.run(new Runnable(){
			@Override
			public void run(){
				particula.remove();
				
			}
		}) ) );
		// basicamente lo mismo que si es tocada 
	}
}
