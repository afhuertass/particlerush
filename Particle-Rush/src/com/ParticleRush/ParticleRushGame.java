package com.ParticleRush;


import com.badlogic.gdx.Game;




public class ParticleRushGame extends Game{
CombosScreen comboScreen;
GameScreen gameScreen;
MainScreen mainScreen;
ReactorScreen reactorScreen;
StatsScreen statsScreen;
StatsDataManager dataManager;
AssetsLoader assetLoader = new AssetsLoader();
	@Override
	public void create() {	
		dataManager = new StatsDataManager();
		if(mainScreen == null){
			
			mainScreen = new MainScreen(this);
		
			this.setScreen(mainScreen);
		}
	}

	@Override
	public void dispose() {
	
	}


}
