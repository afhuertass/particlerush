package com.ParticleRush;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class ReactorScreen implements Screen {
Stage stage;
ParticleRushGame juego;
Group elements;
Group upgradesMenuActors;
Image fondo;
Image scientistBar;
Image upgradesMenu;
OscilatingText reactor;
ImageButton menu;
ImageButton upgrades;
ImageButton hire;
ImageButton sell;
Label energyPerSec;
Label levelReactor;
Label scientist;
Label currentEnergy;
Label earningsPerSale;
Label costoScientist;
Label totalMoney;
// 
Label LEnergyQ;
Label LScienciQ;
Label LEnergyInQ;
Label LCostoQ;
Label LCostoIn;
Label LCostoSc;

Rectangle upgrade1;
Rectangle upgrade2;
Rectangle upgrade3;
boolean isLoaded = false;
int maxSc;
float earning;
float costScientis = 0;
ReactorScreen(ParticleRushGame game){
	this.juego = game;
	juego.assetLoader.loadAtlasReactor();
	}
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
		juego.assetLoader.assetManager.update();
		if(juego.assetLoader.assetManager.isLoaded("data/atlases/reactorAtlas.pack") && !isLoaded && juego.assetLoader.assetManager.isLoaded("data/fuentes/sunnyside.fnt")){
			juego.assetLoader.loadStyle();
			loadElements();
			isLoaded = true;
		}
		
		stage.act(delta);
		stage.draw();
	}
	public void loadElements(){
		elements = new Group();
		upgradesMenuActors = new Group();
		this.fondo = new Image( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack",TextureAtlas.class).findRegion("reactor")));
		this.upgradesMenu = new Image( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack",TextureAtlas.class).findRegion("UpgradesMenu")));
		this.scientistBar = new Image( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack",TextureAtlas.class).findRegion("scientistBar")));
		this.reactor = new OscilatingText(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack",TextureAtlas.class).findRegion("reactorCabecera")));
		this.menu = new ImageButton(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack", TextureAtlas.class).findRegion("menu_reactor")), 
				new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack", TextureAtlas.class).findRegion("menu_reactorOn"))); 
		this.hire = new ImageButton(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack", TextureAtlas.class).findRegion("hire")), 
				new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack", TextureAtlas.class).findRegion("hireOn"))); 
		this.sell = new ImageButton(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack", TextureAtlas.class).findRegion("sell")), 
				new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack", TextureAtlas.class).findRegion("sellOn"))); 
		this.upgrades = new ImageButton(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack", TextureAtlas.class).findRegion("upgrades")), 
				new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/reactorAtlas.pack", TextureAtlas.class).findRegion("upgradesOn"))); 
	
		
	this.levelReactor = new Label("", juego.assetLoader.labelStyle); levelReactor.setPosition(255, 1047);
	this.energyPerSec = new Label("",juego.assetLoader.labelStyle);energyPerSec.setPosition(108	, 942+20);
	this.earningsPerSale = new Label("",juego.assetLoader.labelStyle); earningsPerSale.setPosition(132, 453+20);
	this.currentEnergy = new Label("",juego.assetLoader.labelStyle);currentEnergy.setPosition(336, 528+20);
	this.scientist = new Label("", juego.assetLoader.labelStyle);scientist.setPosition(405, 762);
	this.costoScientist = new Label("", juego.assetLoader.labelStyle); costoScientist.setPosition(330, 684);
	this.totalMoney = new Label("", juego.assetLoader.labelStyle); totalMoney.setPosition(200, 40);
	this.LEnergyInQ = new Label("", juego.assetLoader.labelStyle); this.LEnergyInQ.setVisible(false);
	this.LEnergyQ = new Label("", juego.assetLoader.labelStyle); this.LEnergyQ.setVisible(false);
	this.LScienciQ = new Label("", juego.assetLoader.labelStyle);this.LScienciQ.setVisible(false);
	this.LCostoIn = new Label("", juego.assetLoader.labelStyle); this.LCostoIn.setVisible(false);
	this.LCostoQ = new Label("", juego.assetLoader.labelStyle); this.LCostoQ.setVisible(false);
	this.LCostoSc = new Label("", juego.assetLoader.labelStyle); this.LCostoSc.setVisible(false);
	upgradesMenu.setPosition(30, 1000);float res = upgradesMenu.getHeight()+60;
	upgradesMenu.rotate(-90); upgradesMenu.setVisible(false);
	LEnergyQ.setPosition(upgradesMenu.getX()+449, upgradesMenu.getY()+546-res); LCostoQ.setPosition(upgradesMenu.getX()+120, upgradesMenu.getY()+453-res);
	LEnergyInQ.setPosition(upgradesMenu.getX()+500, upgradesMenu.getY()+180-res); LCostoIn.setPosition(upgradesMenu.getX()+120, upgradesMenu.getY()+95-res);
	LScienciQ.setPosition(upgradesMenu.getX()+499, upgradesMenu.getY()+359-res ); LCostoSc.setPosition(upgradesMenu.getX()+120, upgradesMenu.getY()+273-res);
	upgrade1 = new Rectangle(30,1000-res,0,0);
	scientistBar.setPosition(72, 825);
	reactor.setPosition(69, 1143+100);
	reactor.rotate(-90);
	sell.setPosition(90, 348);
	hire.setPosition(84, 660);
	upgrades.setPosition(93, 168+100); 
	upgrades.rotate(-90);
	upgrades.getChildren().iterator().next().rotate(-90);
	upgrades.getChildren().iterator().next().setPosition(93, 168+100);
	juego.assetLoader.loading.remove();
	elements.addActor(fondo); elements.addActor(reactor); 
	
	elements.addActor(upgrades); elements.addActor(menu);
	elements.addActor(sell); elements.addActor(hire);
	elements.addActor(levelReactor); elements.addActor(energyPerSec); elements.addActor(earningsPerSale);
	elements.addActor(earningsPerSale); elements.addActor(currentEnergy); elements.addActor(scientist);
	elements.addActor(costoScientist); elements.addActor(scientistBar); elements.addActor(totalMoney);
	
	elements.addAction(Actions.fadeOut(0.00001f));elements.addAction(Actions.fadeIn(0.6f));
	upgradesMenuActors.addActor(upgradesMenu);
	upgradesMenuActors.addActor(LEnergyInQ); upgradesMenuActors.addActor(LEnergyQ);
	upgradesMenuActors.addActor(LScienciQ); 
	upgradesMenuActors.addActor(LCostoIn); upgradesMenuActors.addActor(LCostoQ); upgradesMenuActors.addActor(LCostoSc);
	//upgradesMenuActors.setVisible(false);
	stage.addActor(elements);
	stage.addActor(upgradesMenuActors);
	AddListeners();
	loadData();
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		stage.setViewport(720, 1280, true);
		stage.getCamera().position.set(stage.getCamera().position.x - stage.getGutterWidth(), stage.getCamera().position.y,0);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		stage.addActor(juego.assetLoader.loading);
		juego.assetLoader.loadAtlasReactor();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	public void updateData(){
		
	}
	public void loadData(){
		this.levelReactor.setText(Integer.toString(juego.dataManager.reactorLevel));
		this.currentEnergy.setText(Float.toString((float) (Math.rint(juego.dataManager.currentEnergy*100)/100)));
		this.scientist.setText(Integer.toString(juego.dataManager.scientificHired));
		this.totalMoney.setText("$" +Float.toString((float) (Math.rint(juego.dataManager.currentMoney*100)/100)));
		if(this.juego.dataManager.scientificHired < 10) {
			this.costScientis= 400;
		}else {
		this.costScientis = (float) (400 + 100*juego.dataManager.levelScientificQ + Math.pow(juego.dataManager.scientificHired, 2));
		}
		this.costoScientist.setText("-$" +Float.toString(this.costScientis));
		// cargar Energia por segundo
		float energyPs;
		energyPs = (float) (0.3*juego.dataManager.scientificHired*0.8*juego.dataManager.levelScientificQ );
		this.energyPerSec.setText(Float.toString(energyPs)+"/s");
		float earningSale;
		earningSale = (float) (0.8*0.3*juego.dataManager.currentEnergy*juego.dataManager.levelEnegyQ);
		earningSale = (float) Math.rint(earningSale*100)/100;
		this.earningsPerSale.setText(Float.toString(earningSale));
		earning = earningSale;
		// tama�o de la barra;
		int maxScientist = (int)Math.pow(juego.dataManager.reactorLevel,1)*10;
		maxSc = maxScientist;
		float anchoAMostrar = (627 * juego.dataManager.scientificHired)/maxScientist;
		scientistBar.setWidth(anchoAMostrar);
	}
	public void AddListeners(){
		menu.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				juego.mainScreen.isLoaded = false;
				elements.addAction(Actions.sequence(Actions.fadeOut(0.6f) ,  Actions.run(new Runnable(){
					@Override
					public void run(){
						juego.assetLoader.disposeReactorAtlas();
						juego.setScreen(juego.mainScreen);
					}
				})));
				
				
			}
		});
		hire.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				if(hireConditions()){
				juego.dataManager.hireScientist();
				juego.dataManager.spendMoney(costScientis);
				loadData();
				}
			}
		});
		sell.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				// vender, 
				juego.dataManager.earnMoney(earning);
				juego.dataManager.sellEnergy(juego.dataManager.currentEnergy);
				loadData();
			}
		});
		upgrades.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				if(upgradesMenu.isVisible()){
					
					upgradesMenuActors.addAction(Actions.sequence(Actions.fadeOut(0.8f) , Actions.run(new Runnable(){
						@Override
						public void run(){
							upgradesMenu.setVisible(false);
						}
					})));
					return;
				}
				upgradesMenu.setVisible(true);
				for(Actor a : upgradesMenuActors.getChildren()){
					a.setVisible(true);
				}
				upgradesMenuActors.addAction(Actions.sequence(Actions.fadeOut(0.00001f) , Actions.fadeIn(0.5f)));
				loadCostAndLevelsUpgrades();
			}
		});
		upgradesMenu.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				// vender, 
				
				float x2 = upgradesMenu.getWidth()-x;
				Gdx.app.log("Particle", Float.toString(x2));
				if( x2 > 471 && x2< 561){
					// mejorar calidad energia
					if(juego.dataManager.improveEnergyQ()) {
					 } 
				}
				if( x2> 302 && x2 < 375){
					// mejorar calidad cientifica
					if(juego.dataManager.improveScience()) ;
				}
				if(x2 > 117 && x2< 189){
					// mejorar energia in game
					if(juego.dataManager.improveEnergyIn()) ; // sonidos
				}
				loadCostAndLevelsUpgrades();
				loadData();
			}
		});
	}
	public void loadCostAndLevelsUpgrades(){
		float costoEneryQ = (float) ( 100*Math.pow(juego.dataManager.levelEnegyQ, 2));
		float costoScienceQ = (float)( 200*Math.pow(juego.dataManager.levelScientificQ, 2));
		float costoEnergyIn =  (float) (100*Math.pow(juego.dataManager.levelEnergyIn, 2));
		this.LCostoQ.setText(Float.toString(costoEneryQ));
		this.LCostoSc.setText(Float.toString(costoScienceQ));
		this.LCostoIn.setText(Float.toString(costoEnergyIn));
		
		this.LEnergyQ.setText(Integer.toString(juego.dataManager.levelEnegyQ));
		this.LEnergyInQ.setText(Integer.toString(juego.dataManager.levelEnergyIn));
		this.LScienciQ.setText(Integer.toString(juego.dataManager.levelScientificQ));
	}
	public boolean hireConditions(){
		if(juego.dataManager.currentMoney < costScientis){
			return false;
		}
		if(juego.dataManager.scientificHired == maxSc) {
			return false;
		}
		if(juego.dataManager.scientificHired == (maxSc-1)){
			// aumental nivel reactor
			juego.dataManager.improveReactorLevel();
			return true;
		}
		return true;
	}
}
