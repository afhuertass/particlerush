package com.ParticleRush;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class StatsDataManager {
public float totalMoney;
public float totalEnergy;
public float currentEnergy=0;
public float currentMoney=0;
public int reactorLevel=0;
public int scientificHired=0;
public int levelEnegyQ=0;
public int levelScientificQ =0;
public int levelEnergyIn = 0;
Preferences data;
	StatsDataManager(){
		// esta clase maneja los datos guardados
		// total de energia
		// total de dinero
		// nivel reactor // dinero por segundo 
		// cientificos contratados
		// nivel de update 
		// energi quality
		//  energy in game
		// scientific quality
		 data = Gdx.app.getPreferences("particleRush");
		checkBeging();
	}
	public void checkBeging(){
		if(data.getInteger("reactL") == 0) {
		data.putInteger("reactL", 1);
		data.putInteger("scients", 1); 
		data.putInteger("scienceQ", 1);
		data.putInteger("energyQ", 1);
		data.putInteger("energyIn", 1);
		data.putFloat("totalM", 0);
		data.putFloat("totalE", 0);
		data.flush();
		}
		this.reactorLevel = data.getInteger("reactL");
		this.currentEnergy = data.getFloat("currentE");
		this.currentMoney = data.getFloat("currentM");
		this.levelEnegyQ = data.getInteger("energyQ");
		this.levelScientificQ = data.getInteger("scienceQ");
		this.levelEnergyIn = data.getInteger("energyIn");
		this.scientificHired = data.getInteger("scients");
		this.totalMoney = data.getFloat("totalM");
		this.totalEnergy = data.getFloat("totalE");

		data.flush();
	}
	public void improveReactorLevel(){
		//this.reactorLevel = data.getInteger("reactL");
		reactorLevel++;
		data.putInteger("reactL", reactorLevel);data.flush();
	}
	public boolean improveScience(){
		float cost = (float) (200 * Math.pow(this.levelScientificQ,2));
		if(cost <= this.currentMoney){
		spendMoney(cost);
		this.levelScientificQ++;
		data.putInteger("scienceQ", this.levelScientificQ);data.flush();
		return true;
		}
		return false;
	}
	public boolean improveEnergyQ(){
		float cost = (float) (100 * Math.pow(this.levelEnegyQ,2));
		if(cost <= this.currentMoney){
		spendMoney(cost);
		this.levelEnegyQ++;
		data.putInteger("energyQ", this.levelEnegyQ);data.flush();
		return true;
		}
		return false;
	}
	public boolean improveEnergyIn(){
		float cost = (float) (100 * Math.pow(this.levelEnergyIn,2));
		if(cost <= this.currentMoney){
			spendMoney(cost);
		this.levelEnergyIn++;
		data.putInteger("energyIn", this.levelEnergyIn);data.flush();
		return true;
		}
		return false;
	}
	public void hireScientist(){
		this.scientificHired++;
		data.putInteger("scients", this.scientificHired);data.flush();
	}
	public void sellEnergy(float energySold){
		this.currentEnergy -= energySold;
		data.putFloat("currentE", this.currentEnergy);data.flush();
	}
	public void earnEnergy(float energy){
		this.currentEnergy += energy;
		this.totalEnergy += energy;
		data.putFloat("currentE", energy);
		 data.putFloat("totalE",this.totalEnergy); data.flush();
		
	}
	public void earnMoney(float money){
		this.currentMoney += money;
		this.totalMoney += money;
		data.putFloat("currentM", this.currentMoney); 
		data.putFloat("totalM", this.totalMoney); data.flush();
	}
	public void spendMoney(float money){
		this.currentMoney -= money;
		data.putFloat("currentM", this.currentMoney); data.flush();
	}
}
