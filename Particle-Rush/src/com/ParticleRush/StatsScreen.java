package com.ParticleRush;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;


public class StatsScreen implements Screen {
/*vamos a necesitar mas pantallas
 * o mas stats:
 * Energia total:
 * Money Total: 
 * Money Actual:
 * Ganancia por 1 de energia 
 * Ganancia por cientifico
 * Boton para ver las bolitas
 * 
 */
OscilatingText stat;
Image fondo;
ImageButton menu;
ImageButton particleList;
Image login;
Image ranking;
Label totalMoney;
Label totalEnergy; 
Label money;
Label earningsPerEnergy;
Label earningsPerScientist;
Group elements;
ParticleRushGame juego;
Stage stage;

boolean isLoaded = false;
StatsScreen(ParticleRushGame g){
	this.juego = g;
	juego.assetLoader.loadStatsAtlas();
}
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
		juego.assetLoader.assetManager.update();
		if(juego.assetLoader.assetManager.isLoaded("data/atlases/statsAtlas.pack") && !isLoaded && juego.assetLoader.assetManager.isLoaded("data/fuentes/sunnyside.fnt")){
	
			juego.assetLoader.loadStyle();
			loadElements();
			isLoaded = true;
		}
		
		stage.act(delta);
		stage.draw();
	}
	public void loadElements(){
		elements = new Group();
		this.fondo = new Image( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/statsAtlas.pack",TextureAtlas.class).findRegion("stats")));
		this.stat = new OscilatingText( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/statsAtlas.pack" , TextureAtlas.class).findRegion("statsCabecera") ));
		this.menu = new ImageButton(new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/statsAtlas.pack", TextureAtlas.class).findRegion("menu_reactor")), 
				new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/statsAtlas.pack", TextureAtlas.class).findRegion("menu_reactorOn"))); 
		this.login = new Image( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/statsAtlas.pack",TextureAtlas.class).findRegion("login")));
		this.ranking = new Image( new TextureRegionDrawable(juego.assetLoader.assetManager.get("data/atlases/statsAtlas.pack",TextureAtlas.class).findRegion("ranking")));
		this.earningsPerEnergy = new Label("", juego.assetLoader.labelStyle); earningsPerEnergy.setPosition(463, 569-30);
		this.earningsPerScientist = new Label("", juego.assetLoader.labelStyle); earningsPerScientist.setPosition(463, 507-30);
		this.totalEnergy = new Label("", juego.assetLoader.labelStyle); totalEnergy.setPosition(320, 709-30);
		this.totalMoney = new Label("", juego.assetLoader.labelStyle);  totalMoney.setPosition( 320,642-30);
		this.money = new Label("", juego.assetLoader.labelStyle); money.setPosition(209	, 786-30);
		 ranking.rotate(-90); login.setPosition(76, 972);
		ranking.setPosition(368,978+76);
		stat.setPosition(250, 1168+70); stat.rotate(-90); 
		menu.setPosition(500, 10);
		elements.addActor(fondo); elements.addActor(stat); elements.addActor(login);
		elements.addActor(ranking);
		elements.addActor(earningsPerEnergy);
		elements.addActor(earningsPerScientist);
		elements.addActor(totalEnergy); elements.addActor(totalMoney); elements.addActor(money);
		elements.addActor(menu);
		stage.addActor(elements);
		elements.addAction(Actions.sequence(Actions.fadeOut(0.00001f) , Actions.fadeIn(0.5f)));
		loadData();
		juego.assetLoader.loading.remove();
		addListeners();
	}
	public void loadData(){
		float totalM = juego.dataManager.totalMoney;
		float curreM = juego.dataManager.currentMoney;
		float totalE = juego.dataManager.totalEnergy;
		float scienQ = juego.dataManager.levelScientificQ;
		float earnPere = juego.dataManager.levelEnegyQ;
		scienQ *= 0.8f*0.3f; scienQ = (float) Math.rint(scienQ*100)/100;
		earnPere *= 0.8*0.3; earnPere = (float) Math.rint(earnPere*100)/100;
		totalM = (float) Math.rint(totalM*100)/100;
		totalE = (float) Math.rint(totalE*100)/100;
		curreM  = (float) Math.rint(curreM*100)/100;
		this.totalEnergy.setText(Float.toString(totalE)); 
		this.money.setText(Float.toString(curreM));
		this.totalMoney.setText(Float.toString(totalM));
		this.earningsPerScientist.setText(Float.toString(scienQ) + "/s");
		this.earningsPerEnergy.setText("$"+Float.toString(earnPere));
		
	}
	public void addListeners(){
		menu.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event,float x, float y, int pointer,int button ){
			
				return true;
			}
			@Override
			public void touchUp(InputEvent event,float x, float y, int pointer,int button){
				juego.mainScreen.isLoaded = false;
				elements.addAction(Actions.sequence(Actions.fadeOut(0.6f) ,  Actions.run(new Runnable(){
					@Override
					public void run(){
						juego.assetLoader.disposeStatsAtlas();
						
						juego.setScreen(juego.mainScreen);
					}
				})));
				
				
			}
		});
	}
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		stage.setViewport(720, 1280, true);
		stage.getCamera().position.set(stage.getCamera().position.x - stage.getGutterWidth(), stage.getCamera().position.y,0);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		stage.addActor(juego.assetLoader.loading);
		juego.assetLoader.loadStatsAtlas();
		Gdx.app.log("particle", "fuck");
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
